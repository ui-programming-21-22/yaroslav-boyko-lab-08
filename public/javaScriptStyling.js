const canvas = document.getElementById("TheCanvas")
const context = canvas.getContext("2d");

//draw function calls the animate function 

function draw()
{

    animate();
}

//total amount of frames in our spritesheet, this will be specific to youe sprite 
let frames = 6;

//init current frame counter 
let currentFrame = 0;

//sprite 
let sprite = new Image();
sprite.src = "1to6.png";


// Initial time set
let initial = new Date().getTime();
let current; // current time


function animate()
{
    current = new Date().getTime(); //set current time to right now 

    if(current - initial >=5000)
    {
        //check is greater that 750 milliseconds 
        currentFrame = (currentFrame +1)% frames;
        console.log("currentFrame:" + currentFrame);




        initial = current;
    }

    //draw sprite frame 
    context.drawImage(sprite,(sprite.width /6)* currentFrame,0,256,256,0,0,256,256);

}

function gameLoop()
{
    draw();
    window.requestAnimationFrame(gameLoop);
}

window.requestAnimationFrame(gameLoop);