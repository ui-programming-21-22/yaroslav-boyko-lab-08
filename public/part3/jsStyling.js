const canvas  = document.getElementById("TheCanvas")
const context = canvas.getContext("2d");



const scale = 3;
const width = 96;
const height = 32;
const scaledWidth = scale *width;
const scaledHeight = scale * height;

const walkLoop = [0,1,0,2];
const frameLimit = 7;

let currentLoopIndex = 0;
let frameCount = 0;
let currentDirection =0;
let speed = 3;

let character = new Image();
character.src = "hyperlight.png";


function GameObject(spritesheet, x, y, width, heigth)
{
    this.spritesheet = spritesheet;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.mvmDirection = "NONE";
}


let player = new GameObject(character, 0,0, 200,200);


function GameInput(inout)
{
    this.action = input;
}

let gamerInput  = new GameInput("NONE");


function input(event)
{
    if(event.type === "keydown")
    {
        switch (event.keyCode)
        {
            case 37:
                gamerInput = new GameInput("Left");
                break;

            case 38:
                gamerInput = new GameInput("Up");
                break;
            case 39:
                gamerInput = new GameInput("Right");
                break;
            case 40:
                gamerInput = new GameInput("Down");
                break;
            default:
                gamerInput = new GameInput("NOEN");
        }
    }
}


function update()
{
    if(gamerInput.action ==="Up")
    {
        player.y -= speed;
        currentDirection =1;
    }
    else if (gamerInput.action === "Down")
    {
        player.y += speed;
        currentDirection =0;
    }
    else if(gamerInput.action === "Left")
    {
        player.x -= speed;
        currentDirection = 2;
    }
    else if(gamerInput.action === "Rigth")
    {
        player.x += speed;
        currentDirection = 3;

    }
    else if(gamerInput.action === "None")
    {

    }
}


function drawFrame(Image, frameX, frameY, canvasX, canvasY)
{
    context.drawImage(image, frameX * width, frameY * height, width, height,canvasX, canvasY, scaledWidth, scaledHeight);

}

function animation()
{
    if(gamerInput.action != "NONE")
    {
        frameCount++;
        if(frameCount >= frameLimit)
        {
            frameCount =0;
            currentLoopIndex++;
            if(currentLoopIndex >= walkLoop.length)
            {
                currentLoopIndex = 0;
            }
        }
    }
    else{
        currentLoopIndex =0;
    }
    drawFrame(player.spritesheet, walkLoop[currentLoopIndex],currentDirection,player.x, player.y);

}

function draw()
{
    context.clearRect(0,0,canvas.width, canvas.height);
    animation();
}

function gameloop()
{
    update();
    draw();
    window.requestAnimationFrame(gameloop);
}

window.requestAnimationFrame(gameloop);


window.addEventListener('keydown', input);

window.addEventListener('keyup', input);